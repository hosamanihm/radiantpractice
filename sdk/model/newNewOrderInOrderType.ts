/**
 * cake-server
 * cake
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


/**
 * (tsType: @loopback/repository-json-schema#Optional<Omit<NewOrder, \'id\'>, \'typeOfOrderId\'>, schemaOptions: { title: \'NewNewOrderInOrderType\', exclude: [ \'id\' ], optional: [ \'typeOfOrderId\' ] })
 */
export interface NewNewOrderInOrderType { 
    phoneNumber: number;
    altNumber: number;
    address?: string;
    nameOnCake: string;
    orderDate: Date;
    deliveryDateTime: Date;
    typeOfCake?: string;
    price: number;
    advance: number;
    balance: number;
    note?: string;
    images?: string;
    statusId?: number;
    typeOfOrderId?: number;
}

