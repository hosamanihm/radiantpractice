/**
 * cake-server
 * cake
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { NewOrderWithRelations } from './newOrderWithRelations';


/**
 * (tsType: OrderTypeWithRelations, schemaOptions: { includeRelations: true })
 */
export interface OrderTypeWithRelations { 
    id?: number;
    name: string;
    newOrders?: Array<NewOrderWithRelations>;
}

