export * from './newOrderController.service';
import { NewOrderControllerService } from './newOrderController.service';
export * from './newOrderOrderTypeController.service';
import { NewOrderOrderTypeControllerService } from './newOrderOrderTypeController.service';
export * from './newOrderStatusController.service';
import { NewOrderStatusControllerService } from './newOrderStatusController.service';
export * from './orderTypeController.service';
import { OrderTypeControllerService } from './orderTypeController.service';
export * from './orderTypeNewOrderController.service';
import { OrderTypeNewOrderControllerService } from './orderTypeNewOrderController.service';
export * from './pingController.service';
import { PingControllerService } from './pingController.service';
export * from './statusController.service';
import { StatusControllerService } from './statusController.service';
export * from './statusNewOrderController.service';
import { StatusNewOrderControllerService } from './statusNewOrderController.service';
export const APIS = [NewOrderControllerService, NewOrderOrderTypeControllerService, NewOrderStatusControllerService, OrderTypeControllerService, OrderTypeNewOrderControllerService, PingControllerService, StatusControllerService, StatusNewOrderControllerService];
