import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../services/data.service';
import {Issue} from '../../models/issue';
import { StatusControllerService ,NewOrderControllerService, NewOrder, OrderTypeControllerService} from '../../../../sdk';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent {
  statues: any = [];
  selectedStatus : any = 0;
  selectedType : any = 0;
  orderTypes: any = [];
  confirmRegisterForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Issue,
    private statusService: StatusControllerService,
    private typesService: OrderTypeControllerService,
    private newOrderService: NewOrderControllerService,
    private fb: FormBuilder,
              public dataService: DataService) { }
  selected = 'option2';
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  onFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          console.log(event.target.result);
          this.images.push(event.target.result);

          this.myForm.patchValue({
            fileSource: this.images
          });
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  submit() {
    console.log(this.myForm.value);
    this.http.post('http://localhost:8001/upload.php', this.myForm.value)
      .subscribe(res => {
        console.log(res);
        alert('Uploaded Successfully.');
      })
  }
}

  ngOnInit() {
    this.getStatus();
    this.getTypes();
    this.confirmRegisterForm = this.fb.group({
      Name: ['', Validators.compose([
        Validators.required,
      ])
      ],
      phoneNumber: ['', Validators.compose([
        Validators.required,
      ])
      ],
      address: ['', Validators.compose([
        Validators.required,
      ])
      ],
      AlternateNo: [''],
      DeliveryDate: [''],
      DeliveryTime: [''],
      Note: [''],
      Price: [''],
      Balance: [''],
      Advance: [''],
      Type: [''],
        });
  }
  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  
  }

  getStatus(){
    return new Promise((resolve) => {
      this.statusService.statusControllerFind()
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.statues = res;
          }

          resolve(res);
        });
    })
  }
  getTypes(){
    return new Promise((resolve) => {
      this.typesService.orderTypeControllerFind()
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.orderTypes = res;
          }

          resolve(res);
        });
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }


  public confirmAdd() {
    // this.dataService.addIssue(this.data);
    console.log("this.confirmRegisterForm", this.selectedStatus," ",this.selectedType);
    
    return new Promise((resolve) => {
      let request: any = {
        "phoneNumber": this.confirmRegisterForm.controls['phoneNumber'].value,
        "altNumber": this.confirmRegisterForm.controls['AlternateNo'].value,
        "address": this.confirmRegisterForm.controls['address'].value,
        "nameOnCake": this.confirmRegisterForm.controls['Name'].value,
        "typeOfCake": this.confirmRegisterForm.controls['Type'].value,
        "price": this.confirmRegisterForm.controls['Price'].value,
        "advance": this.confirmRegisterForm.controls['Advance'].value,
        "balance": this.confirmRegisterForm.controls['Balance'].value,
        "note": this.confirmRegisterForm.controls['Note'].value,
        "statusId": parseInt(this.selectedStatus),
        "typeOfOrderId": parseInt(this.selectedType),
        "orderDate" : new Date(),
        "deliveryDateTime": "2021-01-19T08:39:10.107Z",
      }
      console.log("this.request", request);
      this.newOrderService.newOrderControllerCreate(request)
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.statues = res;
          }

          resolve(res);
        });
    })
  }
}
