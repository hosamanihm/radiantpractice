import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../services/data.service';
import {Issue} from '../../models/issue';
import { StatusControllerService ,NewOrderControllerService, NewOrder, OrderTypeControllerService} from '../../../../sdk';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent {
  statues: any = [];
  selectedStatus : any = 0;
  selectedType : any = 0;
  orderTypes: any = [];
  confirmRegisterForm: FormGroup;
  public imagePath;
  imgURL: any = [];
  a: Number = 0;
  b: Number = 0;
  c: Number = 0;
  
  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Issue,
    private statusService: StatusControllerService,
    private typesService: OrderTypeControllerService,
    private newOrderService: NewOrderControllerService,
    private fb: FormBuilder,
              public dataService: DataService) { }
  selected = 'option2';
  formControl = new FormControl('', [
    Validators.required
  ]);



  preview(files) {
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL.push( reader.result);
    }
  }



  ngOnInit() {
    this.getStatus();
    this.getTypes();
    this.confirmRegisterForm = this.fb.group({
      Name: ['', Validators.compose([
        Validators.required,
      ])
      ],
      phoneNumber: ['', Validators.compose([
        Validators.required,
      ])
      ],
      address: ['', Validators.compose([
        Validators.required,
      ])
      ],
      AlternateNo: [''],
      DeliveryDate: [''],
      DeliveryTime: [''],
      Note: [''],
      Price: [''],
      Balance: [''],
      Advance: [''],
      Type: [''],
        });
  }
  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  
  }

  getStatus(){
    return new Promise((resolve) => {
      this.statusService.statusControllerFind()
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.statues = res;
          }

          resolve(res);
        });
    })
  }
  getTypes(){
    return new Promise((resolve) => {
      this.typesService.orderTypeControllerFind()
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.orderTypes = res;
          }

          resolve(res);
        });
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  formatedText(event, type: string) {
    console.log('event.target.value.', event.target.value);
    
    switch (type) {
      case 'a':
        this.a = event.target.value;
        this.c = this.compute(this.a, this.b);
        break;
      case 'b':
        this.b = event.target.value;
        this.c = this.compute(this.a, this.b);
        break;
      case 'c':
        this.c = event.target.value;
    }
  }
  compute(a: number, b: number) {
    return String(Number(this.a) - Number(this.b));
  }

  public confirmAdd() {
    // this.dataService.addIssue(this.data);
    
    return new Promise((resolve) => {
      let dateTimeValue = this.confirmRegisterForm.controls['DeliveryDate'].value + " " + this.confirmRegisterForm.controls['DeliveryTime'].value ;
      let yourDate = new Date(dateTimeValue);
      console.log('youtDate',yourDate);
      
      let request: any = {
        "phoneNumber": this.confirmRegisterForm.controls['phoneNumber'].value,
        "altNumber": this.confirmRegisterForm.controls['AlternateNo'].value,
        "address": this.confirmRegisterForm.controls['address'].value,
        "nameOnCake": this.confirmRegisterForm.controls['Name'].value,
        "typeOfCake": this.confirmRegisterForm.controls['Type'].value,
        "price": this.confirmRegisterForm.controls['Price'].value,
        "advance": this.confirmRegisterForm.controls['Advance'].value,
        "balance": this.confirmRegisterForm.controls['Balance'].value,
        "note": this.confirmRegisterForm.controls['Note'].value,
        "statusId": parseInt(this.selectedStatus),
        "typeOfOrderId": parseInt(this.selectedType),
        "orderDate" : new Date(),
        "deliveryDateTime": yourDate,
      }
      console.log("this.request", request);
      // this.newOrderService.newOrderControllerCreate(request)
      //   .subscribe((res: any) => {
      //     console.log("ress", res);
      //     if (res && res.length) {
      //       this.statues = res;
      //     }

      //     resolve(res);
      //   });
    })
  }
}
