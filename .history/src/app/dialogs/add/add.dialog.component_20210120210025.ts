import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../services/data.service';
import {Issue} from '../../models/issue';
import { StatusControllerService ,NewOrderControllerService, NewOrder} from '../../../../sdk';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent {
  statues: any = [];
  confirmRegisterForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Issue,
    private statusService: StatusControllerService,
    private newOrderService: NewOrderControllerService,
    private fb: FormBuilder,
              public dataService: DataService) { }
  selected = 'option2';
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);


  ngOnInit() {
    this.getStatus();
    this.confirmRegisterForm = this.fb.group({
      Name: ['', Validators.compose([
        Validators.required,
      ])
      ],
      phoneNumber: ['', Validators.compose([
        Validators.required,
      ])
      ],
      address: ['', Validators.compose([
        Validators.required,
      ])
      ],
      AlternateNo: [''],
      Note: [''],
      Price: [''],
      Balance: [''],
      Advance: [''],
      Type: [''],
        });
  }
  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  
  }

  getStatus(){
    return new Promise((resolve) => {
      this.statusService.statusControllerFind()
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.statues = res;
          }

          resolve(res);
        });
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd() {
    // this.dataService.addIssue(this.data);
    console.log("this.confirmRegisterForm" ,this.confirmRegisterForm.controls['phoneNumber'].value);
    
    return new Promise((resolve) => {
      let request: any = {
        "phoneNumber": this.confirmRegisterForm.controls['phoneNumber'].value,
        "altNumber": this.confirmRegisterForm.controls['AlternateNo'].value,
        "address": this.confirmRegisterForm.controls['Address'].value,
        "nameOnCake": this.confirmRegisterForm.controls['Name'].value,
        "typeOfCake": this.confirmRegisterForm.controls['Type'].value,
        "price": this.confirmRegisterForm.controls['Price'].value,
        "advance": this.confirmRegisterForm.controls['Advance'].value,
        "balance": this.confirmRegisterForm.controls['Balance'].value,
        "note": this.confirmRegisterForm.controls['Note'].value,
        "statusId": 0,
        "typeOfOrderId": 0,
        "deliveryDateTime": "2021-01-19T08:39:10.107Z",
      }
      this.newOrderService.newOrderControllerCreate(request)
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.statues = res;
          }

          resolve(res);
        });
    })
  }
}
