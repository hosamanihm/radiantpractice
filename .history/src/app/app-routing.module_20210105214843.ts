// Angular
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UnauthorizedComponent} from './unauthorized/unauthorized.component';
import { TermAndConditionComponent } from './term-and-condition/term-and-condition.component';
const routes: Routes = [
	{path: 'auth', loadChildren: () => import('app/views/pages/auth/auth.module').then(m => m.AuthModule)},
	{path: 'unauthorized', component: UnauthorizedComponent},
	{path: 'terms-and-conditions', component: TermAndConditionComponent},
	// enable this router to set which demo theme to load,
	{path: '', loadChildren: () => import('app/views/themes/inqude/theme.module').then(m => m.ThemeModule)},
	{path: '**', redirectTo: 'error/403', pathMatch: 'full'},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	declarations: [UnauthorizedComponent,
		TermAndConditionComponent],
	exports: [RouterModule]
})
export class AppRoutingModule
{
}
