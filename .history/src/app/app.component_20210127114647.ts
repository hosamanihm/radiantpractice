import { Component, ElementRef, OnInit, ViewChild, ChangeDetectorRef} from '@angular/core';
import {DataService} from './services/data.service';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Issue} from './models/issue';
import {DataSource} from '@angular/cdk/collections';
import { jsPDF } from 'jspdf';

import {AddDialogComponent} from './dialogs/add/add.dialog.component';
// import {EditDialogComponent} from './dialogs/edit/edit.dialog.component';
// import {DeleteDialogComponent} from './dialogs/delete/delete.dialog.component';
import {BehaviorSubject, fromEvent, merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import { NewOrderControllerService, StatusControllerService } from '../../sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  displayedColumns = ['Name on cake', 'Phone Number', 'Type Of Cake', 'Balance', 'Type of order','Status'];
  exampleDatabase: DataService | null;
  dataSource: any;
  index: number;
  id: number;
  selectedStatus = "pending";
  statues: any = [];

  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
    private cd: ChangeDetectorRef,
    private statusService: StatusControllerService,
    private planService: NewOrderControllerService,
              public dataService: DataService) {}

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter',  {static: true}) filter: ElementRef;

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  
  ngOnInit() {
    this.getStatus();
    this.loadData();
  }

  onStatusChanged(event,row){
    console.log('event',event.value);
    console.log('row',row.id);
    let req = row;
    req.statusId = row.id;
    return new Promise((resolve) => {
      this.planService.newOrderControllerUpdateById(row.id,{statusId : row.id})
        .subscribe((res: any) => {
          console.log("ress", res);

          resolve(res);
        });
    })

  }

  refresh() {
    this.loadData();
  }

  public downloadAsPDF() {
    const doc = new jsPDF();

    doc.text('Hello world', 20, 20);

    doc.save('tableToPdf.pdf');
  }

  getStatus() {
    return new Promise((resolve) => {
      this.statusService.statusControllerFind()
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.statues = res;
          }

          resolve(res);
          this.cd.detectChanges();
        });
    })
  }
  addNew() {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: {issue: Issue }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        this.exampleDatabase.dataChange.value.push(this.dataService.getDialogData());
        this.refreshTable();
      }
    });
  }





  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);
  }





  public loadData() {
 
      return new Promise((resolve) => {
        this.planService.newOrderControllerFindByFilter()
          .subscribe((res: any) => {
            console.log("ress",res);
            
            resolve(res);
            this.dataSource = res;
          });
      })
    }
}
