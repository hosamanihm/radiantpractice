import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { DataService } from '../../services/data.service';
import { NewOrderControllerService, StatusControllerService } from '../../../../sdk';


@Component({
  selector: 'app-delete.dialog',
  templateUrl: '../../dialogs/delete/delete.dialog.html',
  styleUrls: ['../../dialogs/delete/delete.dialog.css']
})
export class DeleteDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>,
    private planService: NewOrderControllerService,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: DataService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete() {
    return new Promise((resolve) => {
      this.planService.newOrderControllerDeleteById(this.data.id)
        .subscribe((res: any) => {
          console.log("ress", res);

          resolve(res);
          this.dataService.deleteIssue(this.data.id);
        });
    })
  }
}