import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, ChangeDetectorRef} from '@angular/core';
import {DataService} from '../../services/data.service';
import {Issue} from '../../models/issue';
import { NewOrderControllerService, StatusControllerService,OrderTypeControllerService } from '../../../../sdk';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {jsPDF} from 'jspdf';
import html2canvas from 'html2canvas'; 
import * as moment from 'moment';
export default moment;
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { ViewReceiptDialogComponent } from '../viewReceipt/viewReceipt.dialog.component';



@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent {
  statues: any = [];
  editObj: any = {};
  selectedStatus : any = 2;
  selectedType : any = 0;
  orderTypes: any = [];
  deliverDate: any = '';
  deliverTime: any = '';
  confirmRegisterForm: FormGroup;
  public imagePath;
  imgURL: any = [];
  a: number = 0;
  b: number = 0;
  c: Number = 0;
  todayDate: any = moment(new Date()).format('DD-MM-YYYY');
  phoneNo: Number = 0;
  anyChanges = false;

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private cd: ChangeDetectorRef,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private statusService: StatusControllerService,
    private typesService: OrderTypeControllerService,
    private newOrderService: NewOrderControllerService,
    private fb: FormBuilder,
              public dataService: DataService) {
              
               }
  selected = 'option2';
  formControl = new FormControl('', [
    Validators.required
  ]);



  preview(files) {
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL.push( reader.result);
    }
  }


  ngOnInit() {
    this.getStatus();
    this.getTypes();

    this.confirmRegisterForm = this.fb.group({
      Name: ['', Validators.compose([
        Validators.required,
      ])
      ],
      phoneNumber: ['', Validators.compose([

        Validators.required, 

      ])
      ],
      address: ['', Validators.compose([
        Validators.required,
      ])
      ],
      AlternateNo: [''],
      DeliveryDate: ['', Validators.compose([
        Validators.required,
      ])],
      DeliveryTime: ['', Validators.compose([
        Validators.required,
      ])],
      Note: [''],
      Price: ['', Validators.compose([
        Validators.required,
      ])],
      Balance: [''],
      selectedStatus: [''],
      selectedType: [''],
      Advance: [''],
      Type: [''],
        });
        if(this.data !=null){

          this.confirmRegisterForm.valueChanges.subscribe(dt => this.fieldChanges());
        }
  }
  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  fieldChanges() {
    
    let nameOnCake = this.confirmRegisterForm.controls['nameOnCake'] && this.confirmRegisterForm.controls['nameOnCake'].value || this.data.nameOnCake;
    let phoneNumber = this.confirmRegisterForm.controls['phoneNumber'] && this.confirmRegisterForm.controls['phoneNumber'].value || this.data.phoneNumber;
    let address = this.confirmRegisterForm.controls['address'] && this.confirmRegisterForm.controls['address'].value || this.data.address;
    let AlternateNo = this.confirmRegisterForm.controls['AlternateNo'] && this.confirmRegisterForm.controls['AlternateNo'].value || this.data.altNumber;
    let Note = this.confirmRegisterForm.controls['Note'] && this.confirmRegisterForm.controls['Note'].value || this.data.note;
    let selectedStatus = this.confirmRegisterForm.controls['selectedStatus'] && this.confirmRegisterForm.controls['selectedStatus'].value || this.data.statusId;
    let selectedType = this.confirmRegisterForm.controls['selectedType'] && this.confirmRegisterForm.controls['selectedType'].value || this.data.typeOfOrderId;
    let Type = this.confirmRegisterForm.controls['Type'] && this.confirmRegisterForm.controls['Type'].value || this.data.type;
    let dateValue = this.confirmRegisterForm.controls['DeliveryDate'].value || this.deliverDate;
    let timeValue =  this.confirmRegisterForm.controls['DeliveryTime'].value || this.deliverTime;
  


    
    this.editObj = 
    {
      nameOnCake: nameOnCake.trim(),
  phoneNumber : parseInt(phoneNumber.trim()),
  address : address,
      altNumber: parseInt(AlternateNo),
      deliveryDateTime: dateValue +" "+ timeValue,
  orderDate: this.data.orderDate,
      note: Note ,
      price: this.confirmRegisterForm.controls['Price'].value || this.a,
      balance: this.confirmRegisterForm.controls['Balance'].value || this.c,
  statusId : selectedStatus,
  typeOfOrderId : selectedType,
      advance: this.confirmRegisterForm.controls['Advance'].value || this.b,
  typeOfCake : Type
}



  }
  
  submit() {
  
  }

  getStatus(){
    return new Promise((resolve) => {
      this.statusService.statusControllerFind()
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.statues = res;
          }
          if (this.data) {
            console.log("this.data.statusId", this.data.statusId);
            this.selectedStatus = this.data.statusId;
            this.cd.detectChanges();
            console.log(' this.selectedStatus', this.selectedStatus);
          }
          resolve(res);
        });
    })
  }
  getTypes(){
    return new Promise((resolve) => {
      this.typesService.orderTypeControllerFind()
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.orderTypes = res;
          }
          if (this.data) {
            this.selectedType = this.data.typeOfOrderId;
            this.a = this.data.price;
            this.b = this.data.advance;
            this.c = this.data.balance;
            console.log(' this.selectedType', this.selectedType);
            this.deliverDate = moment(this.data.deliveryDateTime).format('yyyy-MM-DD');
            this.deliverTime = moment(this.data.deliveryDateTime).format('hh:mm');
            console.log(' this.deliverDate', moment(this.data.deliveryDateTime).format('DD/MM/YYYY'));
            this.cd.detectChanges();
          }
          resolve(res);
        });
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  formatedText(event, type: string) {
    console.log('event.target.value.', event.target.value);
    
    switch (type) {
      case 'a':
        this.a = event.target.value;
        this.c = this.compute(this.a, this.b);
        break;
      case 'b':
        this.b = event.target.value;
        this.c = this.compute(this.a, this.b);
        break;
      case 'c':
        this.c = event.target.value;
    }
  }
  compute(a: number, b: number) {
    return ((this.a) - (this.b));
    }
  onStatusChanged(event : any){
this.selectedType = event.value;
  }
 
  public confirmAdd() {
    
    return new Promise((resolve) => {
     
      if(this.data != null ){
        // const dialogRef = this.dialog.open(ViewReceiptDialogComponent, { data: this.data });
        console.log('this.editObj.deliveryDateTime', this.editObj.deliveryDateTime.trim());
        if (this.editObj.deliveryDateTime.trim() == null){
          this.editObj.deliveryDateTime = new Date(this.deliverDate + " " + this.deliverTime).toISOString();
        }else{
          this.editObj.deliveryDateTime = new Date(this.editObj.deliveryDateTime).toISOString();

        }
        this.newOrderService.newOrderControllerReplaceById(this.data.id,this.editObj)
        .subscribe((res: any) => {
          console.log("ress", res);
          this.toastr.success('Order updated successfully!!', '', {
            positionClass: 'toast-bottom-right',
          });
          resolve(res);
        });
     
      
      }else{
        let dateTimeValue = this.confirmRegisterForm.controls['DeliveryDate'].value + " " + this.confirmRegisterForm.controls['DeliveryTime'].value;
        console.log('dateTimeValue', dateTimeValue);
      
        if(dateTimeValue == null){
          dateTimeValue = this.deliverDate + " " + this.deliverTime;
        }
        let yourDate = new Date(dateTimeValue);

        let request: any = {
          "phoneNumber": this.confirmRegisterForm.controls['phoneNumber'].value,
          "altNumber": this.confirmRegisterForm.controls['AlternateNo'].value,
          "address": this.confirmRegisterForm.controls['address'].value,
          "nameOnCake": this.confirmRegisterForm.controls['Name'].value,
          "typeOfCake": this.confirmRegisterForm.controls['Type'].value,
          "price": this.confirmRegisterForm.controls['Price'].value,
          "advance": this.confirmRegisterForm.controls['Advance'].value,
          "balance": this.c,
          "note": this.confirmRegisterForm.controls['Note'].value,
          "statusId": parseInt(this.selectedStatus),
          "typeOfOrderId": parseInt(this.selectedType),
          "orderDate": new Date().toISOString(),
          "deliveryDateTime": yourDate.toISOString(),
        }
        
        this.newOrderService.newOrderControllerCreate(request)
          .subscribe((res: any) => {
            console.log("ress", res);
            if (res && res.length) {
              
              this.data = res;
            }
            

            

            resolve(res);
            const dialogRef = this.dialog.open(ViewReceiptDialogComponent, { data: res });
          });
      }
     
    })
  }
}
