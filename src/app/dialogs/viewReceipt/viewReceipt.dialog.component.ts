import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { DataService } from '../../services/data.service';
import { NewOrderControllerService, StatusControllerService } from '../../../../sdk';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
export default moment;
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  templateUrl: '../../dialogs/viewReceipt/viewReceipt.dialog.html',
  styleUrls: ['../../dialogs/viewReceipt/viewReceipt.dialog.css']
})

export class ViewReceiptDialogComponent 
{

  constructor(public dialogRef: MatDialogRef<ViewReceiptDialogComponent>,
    private planService: NewOrderControllerService,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: DataService) { }
  todayDate: any = moment(new Date()).format('DD-MM-YYYY');
  deliverDate: any = moment(this.data.deliveryDateTime).format('DD-MM-YYYY');
  deliverTime: any = moment(this.data.deliveryDateTime).format('HH:mm a');
  
  public downloadData() {
    var data = document.getElementById('parentdiv');

    html2canvas(data).then(canvas => {
      console.log("testing dialog windowkkkkkkkkkkkkkkkkkkkk")
      // Few necessary setting options  
      let imgWidth = 190;
      let pageHeight = 295;
      let imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      let position = -5;
      pdf.addImage(contentDataURL, 'PNG', 10, position, imgWidth, imgHeight)
      pdf.save('MYPdf.pdf'); // Generated PDF   
      // this.dialogRef.close();
    });
  }
 
  ngOnInit(){
    console.log('data', this.data);
    
    // this.captureScreen();
  }
  onPrint()
  {
    window.print();
  }

}