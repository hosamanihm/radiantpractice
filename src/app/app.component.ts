import { Component, ElementRef, OnInit, ViewChild, ChangeDetectorRef} from '@angular/core';
import {DataService} from './services/data.service';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { jsPDF } from 'jspdf';
import { ToastrService } from 'ngx-toastr';

import {AddDialogComponent} from './dialogs/add/add.dialog.component';
import {BehaviorSubject, fromEvent, merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { environment } from '../environments/environment';

import { NewOrderControllerService, StatusControllerService } from '../../sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  displayedColumns = ['Name on cake', 'Phone Number', 'Type Of Cake', 'Balance', 'Type of order','Status','actions'];
  exampleDatabase: DataService | null;
  dataSource: any;
  index: number;
  id: number;
  selectedStatus = "pending";
  statues: any = [];
  panelOpenState = false;
  filterStatus : any ;
  filterFromDate : any ;
  filterToDate : any ;
  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,
    private cd: ChangeDetectorRef,
    private toastr: ToastrService,
    private statusService: StatusControllerService,
    private planService: NewOrderControllerService,
              public dataService: DataService) {}

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter',  {static: true}) filter: ElementRef;

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;
  
  ngOnInit() {
    this.getStatus();
    this.loadData();
  }
  

  onFilterStatus(event ){
    this.filterStatus = event.value;
  }
  onSearch(){
    console.log('event', this. filterFromDate);
    let statusString = null;
    if(this.filterStatus != undefined){
      for(let i=0;i<this.statues.length;i++){
        if (this.statues[i].id == this.filterStatus){
          
          statusString = this.statues[i].name;
        }
      }
    }
    return new Promise((resolve) => {
      this.planService.newOrderControllerFindByFilter(null, this.filterFromDate || null, this.filterToDate || null, statusString)
        .subscribe((res: any) => {
          console.log("ress", res);

          resolve(res);
          this.dataSource = res;

        });
    })
  }
  startEdit(row : any) {
    // index row is used just for debugging proposes and can be removed
    console.log(row);
    const dialogRef = this.dialog.open(AddDialogComponent, { disableClose: true ,
      data: row
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
        this.refreshTable();
      }
    });
  }

  deleteItem(id: number,nameOnCake : string,phoneNumber : any) {
    // this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: { id: id, nameOnCake: nameOnCake, phoneNumber: phoneNumber}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
        // const foundIndex = this.exampleDatabase.dataChange.value.findIndex(x => x.id === this.id);
        // for delete we use splice in order to remove single object from DataService
        this.refreshTable();
      }
    });
  }

  downloadData(){
    document.location.href = environment.BASE_PATH + '/new-orders/download-details';
  }
  onStatusChanged(event,row){
    console.log('event',event.value);
    console.log('row',row.id);
    return new Promise((resolve) => {
      this.planService.newOrderControllerUpdateById(row.id, { statusId: event.value})
        .subscribe((res: any) => {
          console.log("ress", res);

          resolve(res);
          this.toastr.success('Status updated successfully!!', '', {
            positionClass: 'toast-bottom-right',
          });
        });
    })

  }

  refresh() {
    this.loadData();
  }

  public downloadAsPDF() {
    const doc = new jsPDF();

    doc.text('Hello world', 20, 20);

    doc.save('tableToPdf.pdf');
  }

  getStatus() {
    return new Promise((resolve) => {
      this.statusService.statusControllerFind()
        .subscribe((res: any) => {
          console.log("ress", res);
          if (res && res.length) {
            this.statues = res;
          }

          resolve(res);
          this.cd.detectChanges();
        });
    })
  }
  addNew() {
    // const dialogRef = this.dialog.open(AddDialogComponent, {
    //   data: {issue: Issue }
    // });
    const dialogRef =  this.dialog.open(AddDialogComponent, { disableClose: true });

    dialogRef.afterClosed().subscribe(result => {
      console.log('result',result);
      
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside DataService
        // this.exampleDatabase.dataChange.value.push(this.dataService.getDialogData());
        this.loadData();
      }
    });
  }

  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);
  }





  public loadData() {
 
      return new Promise((resolve) => {
        this.planService.newOrderControllerFindByFilter()
          .subscribe((res: any) => {
            console.log("ress",res);
            
            resolve(res);
            this.dataSource = res;
          });
      })
    }
}
